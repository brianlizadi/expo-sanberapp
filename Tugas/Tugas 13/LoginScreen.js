
import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, Button } from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

export default class App extends React.Component {
    render(){
          return (
            <View style={styles.container}>
                <View style={styles.boxIsiJudul}>
                    <Image source={require('./image/logo1.png')} style={{width: 250, height: 90}}/> 
                </View>
                
                <View style={styles.boxIsiHalaman}>
                    <Text style={{marginBottom:10, fontSize:20, color:'#003366'}}>Register</Text>
                        <View style={{flex: 1, marginTop:15}}>
                            <Text style={{flexDirection:'column', justifyContent:'flex-start', color:'#3EC6FF'}}>
                                Username</Text>
                            <View style={{flexDirection:"row", justifyContent:'flex-start'}}>
                                <View style={styles.boxIcon}>
                                    <Icon name="user" size={'{30}'} color="#003366" />
                                </View>
                                <TextInput style={styles.boxIsiinputteks} />
                            </View>
                        </View>
                        <View style={{flex: 1, marginTop: 30}}>
                            <Text style={{flexDirection:'column', justifyContent:'flex-start', color:'#3EC6FF'}}>
                                Password</Text>
                            <View style={{flexDirection:"row", justifyContent:'flex-start'}}>
                                <View style={styles.boxIcon}>
                                    <Icon name="key" size={'{30}'} color="#003366" />
                                </View>
                                <TextInput style={styles.boxIsiinputteks} />
                            </View>
                        </View>
                
                </View>
                <View style={styles.boxIsiSignin}>
                    <View style={styles.buttonBox}>
                        <Button title="Masuk" color="#3EC6FF"/>
                            <Text style={{padding:5}}>Or</Text>
                        <Button title="Daftar" color="#003366"/>
                    </View>
                </View>
            </View>
  );}
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
  boxIsiJudul: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red'
 
  },
  boxIsiHalaman: {
    flex: 1,
    paddingBottom: 20,
    alignItems: "center",
  },
  boxIsiSignin: {
    flex: 2,
    paddingTop : 10,
  },
  boxIsiinputteks: {
    height: 40,
    width: 220,
    backgroundColor: '#ededed',
    borderBottomColor:'#003366',
    borderBottomWidth : 2,
    marginBottom: 15
  },
  buttonBox: {
      flex:1,
      marginTop:10,
      padding:15,
      justifyContent: 'flex-start',
      alignItems: 'center'
  },
  boxIcon:{
    height: 40, 
    width: 35,
    backgroundColor: '#ededed',
    justifyContent:'center', 
    alignItems: 'center'
},
});


